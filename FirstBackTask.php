<?php
/**
 *Подсчёт количества отправленных сообщений
 *Необходимо подсчитать сколько всего было удачных сеансов связи с отправленными сообщениями.
 *Для отправки сообщений вам всегда потребуется строгая последовательность команд: start — connect — message — end.
 *
 *
 *Команды означают следующее:
 *
 *• start — начать сеанс связи.
 *
 *• connect — установить соединение.
 *
 *• message — отправить сообщение.
 *
 *• end — завершить сеанс связь.
 *
 *
 *Входящие параметры:
 *
 * actions — строковый массив команд, размер массива должен быть больше 3 и меньше 50
 * Каждый элемент может принимать только одно из 4 значений (“start” | “connect” | “message” | “end”)
 *
 *Результат:
 *
 * целое число — количество сеансов связи, в которые отправлялись сообщения
 *
 *
 *Пример 1 (PHP):
 *
 *$actions = ["start", "connect", "message", "end"];
 *
 *FirstBackTask::getResult(actions);  //1, одно сообщение в одну сессию
 *
 *
 *Пример 2 (PHP):
 *
 *$actions = ["start", "connect", "message", "end", "start", "connect", "message", "end", "start", "connect", "message"];
 *
 *FirstBackTask::getResult(actions); // 2
 * (два сообщения, по одному в первой и второй сессии, третья сессия не была завершена)
 */

class FirstBackTask
{
    static function getResult(array $actions): int {
        // check if actions array has valid size
        if (count($actions) < 4 || count($actions) > 50) {
            throw new Error(sprintf("Invalid \$action array length: %d\n", count($actions)));
        }
        // check if actions array contains only valid actions
        foreach ($actions as $p => $a) {
            if (!in_array($a, ['start', 'connect', 'message', 'end'])) {
                throw new Error("Invalid action \"$a\" at \$actions[$p]\n");
            }
        }
        $sessionSequence = 'startconnectmessageend';
        return substr_count(implode("", $actions), $sessionSequence);
    }
}