<?php
/**
*Поиск последней цифры результата возведения в степень
*Необходимо вернуть последнюю цифру результата возведения в степень (a^b).
*
*Входящие параметры:
*• a, целое число, 1<a<1000000000
*• b, целое число, 1<b<1000000000
*Результат:
*• целое число — последнее число результата подсчёта
*
*Пример 1 (PHP):
*$a=7;
*$b=3;
*SecondBackTask::getResult(a,b); //3
*
*Пример 2 (PHP):
*$a=5;
*$b=123456789;
*SecondBackTask::getResult(a,b)); //5
*/

class SecondBackTask
{
    static function getResult(int $base, int $exp): int {
        if ($base < 1 || $base > 1000000000 || $exp < 1 || $exp > 1000000000) {
            throw new Error(sprintf("Parameters(base=%d, exp=%d) not in range (1, 1000000000)\n", $base, $exp));
        }

        $baseLastDigit = (int)(((string)$base)[-1]);
        // unnecessary optimization
        if (in_array($baseLastDigit, [0, 1, 5, 6])) {
            return $baseLastDigit;
        }

        $rem = $exp % 4;
        if ($rem === 0 ) {
            if (in_array($baseLastDigit, [2, 4, 6, 8])) {
                return 6;
            }
            if (in_array($baseLastDigit, [3, 7, 9])) {
                return 1;
            }
        }
        $baseLastDigit = pow($baseLastDigit, $rem);
        return (int)(((string)$baseLastDigit)[-1]);
    }
}