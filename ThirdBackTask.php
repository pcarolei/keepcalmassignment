<?php

/**
 *Преобразование строки

*Дана строка с набором чисел и фигурных скобок.
*Каждая цифра перед фигурными скобками определяет количество повторений данных внутри фигурных скобок.
*Хотя скобки могут быть вложенными, может быть не более пяти слоев вложенности.
*Необходимо преобразовать строку в соответствующую последовательность чисел.
*
*Входящие параметры:
* строка — длина строки от 1 до 25 символов, каждый символ в строке может принимать одно из значений (“{“|”}”|”1”|”2”|”3”|”4”|”5”|”6”|”7”|”8”|”9”).
*
*Нет случаев неправильных данных, например, когда число не стоит перед фигурными скобками или фигурная скобка не закрывается
*
*Результат:
* строка — состоящая из последовательности чисел, сформированная по обозначенным правилам
*
*
*Пример 1 (PHP):
*$s="2{4}3{23}";
*ThirdBackTask::getResult(s); //"44232323"
*
*Пример 2 (PHP):
*$s="4{93{2}}";
*ThirdBackTask::getResult(s); //"9222922292229222"
*/

class ThirdBackTask
{
    static function getResult(string $str, bool $recursionCall = false): string {
        if (!$recursionCall && (empty($str) || strlen($str) > 25)) {
            throw new Error(sprintf("String %s len is not in range (1, 25)\n", $str));
        }

        $pattern = '/\d\{\d+\}/';
        if (!preg_match_all($pattern, $str, $matches, PREG_OFFSET_CAPTURE)) {
            return $str;
        } else {
            foreach ($matches[0] as $m) {
                $newSubstr = str_repeat(substr($m[0], 2, -1), $m[0][0]);
                $str = str_replace($m[0], $newSubstr, $str);
                if (strpos($str, '{') !== false) {
                    return self::getResult($str, true);
                }
                return $str;
            }
        }
    }
}